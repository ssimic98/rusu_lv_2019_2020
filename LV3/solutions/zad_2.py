import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')

#prvi
#plt.figure(1)
mtcars1 = mtcars.groupby('cyl').mean()
plt.bar([4, 6, 8], mtcars1['mpg'], align='center', alpha= 0.5)
plt.show()

# drugi
mtcars2 = mtcars[['cyl', 'wt']]
mtcars2.boxplot(by='cyl')


#treci
#mtcars3 = mtcars.groupby('am').mean()
#print(mtcars3)
#plt.bar([0, 1], mtcars3['mpg'], align='center', alpha=1, linewidth = 0.1)
#plt.show()


#cetvrti
plt.figure(4)
plt.scatter(mtcars[mtcars.am==1].hp, mtcars[mtcars.am==1].mpg, color='r' )
plt.scatter(mtcars[mtcars.am==0].hp, mtcars[mtcars.am==0].mpg, color='b')
plt.xlabel('Ubrzanje (konjske snage')
plt.ylabel('Potrosnja (miles/galon)')
plt.legend(['Rucni mjenjac','Automatski mjenjac'])
plt.show()

print(mtcars.tail(4))