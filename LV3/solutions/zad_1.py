import numpy as np
import pandas as pd


mtcars = pd.read_csv('../resources/mtcars.csv') 


#prvi
mtcars.sort_values(by=['mpg'], inplace=True)
print(mtcars.car.tail(5)) 

#drugi
mtcars.sort_values(by=['mpg'], inplace=True)
mtcars8=mtcars[(mtcars.cyl==8)].car.head(3)
print(mtcars8)

#treci
mtcars6= mtcars[mtcars.cyl == 6].mpg.mean()
print('Srednja vrijednost potrosnje je: ',mtcars6)

#cetvrti
mtcars4 = mtcars[(mtcars.cyl == 4) & (mtcars.wt >= 2.0) & (mtcars.wt <= 2.2)].mpg.mean()
print('Srednja vrijednost potrosnje je: ',mtcars4)

#peti
automatic_transsmision = mtcars[mtcars.am == 0].car.count()
manual_transsmision=mtcars[mtcars.am==1].car.count()
print('Automobila s automatic transmissionom ima: ', automatic_transsmision)
print('Automobila s manual transmissionom ima:', manual_transsmision)

#sesti
mtcars100 = mtcars[(mtcars.am == 0)&(mtcars.hp > 100)].car.count()
print('Automobila s automatskim mjenjacem i preko 100 konjskih snaga ima: ', mtcars100)

#sedmi
mtcars['kg'] = mtcars["wt"] * 0.45359237 * 1000
print(mtcars[['car','kg']]) 