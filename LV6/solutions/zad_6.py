import matplotlib.pyplot as plt
import numpy as np
import sklearn.linear_model as lin
import sklearn.metrics as metrics

def plot_confusion_matrix(c_matrix):
    
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()


def generate_data(n):
    
    #prva klasa
    n1 = n // 2
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1))); 
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = n - n // 2
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n) 
    data = data[indices,:]
    
    return data


#plt.scatter(test[:, 0], test[:, 1], c=test[:, 2])


np.random.seed(242)
trening_podaci=generate_data(200)

np.random.seed(242)
test_podaci=generate_data(100)

model=lin.LogisticRegression().fit(test_podaci[:,:2],test_podaci[:,2])
test_ocekivani=test_podaci[:,2]
test_rezultat=model.predict(test_podaci[:,:2])

matrica_zabune = metrics.confusion_matrix(test_ocekivani, test_rezultat)
plot_confusion_matrix(matrica_zabune)
acc = metrics.accuracy_score(test_ocekivani, test_rezultat)
prec = metrics.average_precision_score(test_ocekivani, test_rezultat)
recall = metrics.recall_score(test_ocekivani, test_rezultat)
_, recall_all, _, _ = metrics.precision_recall_fscore_support(test_ocekivani, test_rezultat)
sensitivity = recall_all[1]
specificity = recall_all[0]
missclasification = 1 - acc
