Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.
Prvo sam umjesto raw_input napisalio samo input.
Zatim sam promjenili ime fnamex u fname.
Potom sam stavio sve u zagradu kada pozivamo print.

U ovoj vježbi upoznali smo se sa Python jezikom kao i radom Gita.
Proučeni su primjeri koji su pomogli pri rješavanju zadataka kao i način na koji se
pišu određene naredbe kao što je if, for, kao i kako se definiraju funkcije. Koristili smo program
Spyder IDE za rad s Pythonom. U prvom zadatku smo učitali dva broja koja koja smo izračunali i ispisano je ukupno koliko radnik dobija
za određeni broj sati.
Drugi zadatak smo pomoću if else odredili interval u kojem broj može biti i na ekran ispisali kojoj kategoriji broj pripada
ako ne pripada tada se ispisuje da broj nije u intervalu. Treći zadatak je kao i prvo samo što smo definirali funkciju koja nam
vraća koliko je ukupno zaradio. U četvrtom zadatku korisinik unosi brojeve sve dok ne upiše done. U slučaju da želi upisat slovo
na ekran mu ispisuje da to nije broj. To smo postigli uz pomoć try-except naredbe. Nakon što upiše done korisinik dobija 
najveći, najmanji, srednju vrijednost i ukupan broj brojeva.U petom zadatku otvaramo datoteku i sve dok ne dođe do X-DSPAM-Confidence
nastavljai tražiti tu liniju. Na kraju kad ju je našao zbroji i ispisujemo prosjek. U šestom zadatku stvaramo liste email i hostname.
Otvaramo datoteku i ako linija počinje sa from spremamo to u email listu. Potom stvaramo email dictionary u kojeg spremamo hostnameove.
U email listi tražimo string koji sadrži @ i tada to spremamo u hostname listu. I na kraju provjeravamo u Hostname listi sadrži li 
u hostnameListi i ako postoji povećavamo brojac za pojedine hostnameove.
Riješeni su zadaci prema uputama iz predloška. 