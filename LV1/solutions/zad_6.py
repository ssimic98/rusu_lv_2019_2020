EmaiLista = []
HostnameLista = []
uct_brojac=0
media_brojac=0
umich_brojac=0
iupui_brojac=0
carnet_brojac=0
gmail_brojac=0

datoteka = input('Upisi ime datoteke: ')
fhand = open(datoteka)
for line in fhand:   
    line = line.rstrip( )   
    if line.startswith('From:'):
        EmaiLista.append(line[6:])
        
email = dict()

email['uct'] = 'uct.ac.za'
email['media']= 'media.berkeley.edu'
email['umich']= 'umich.edu'
email['iupui']= 'iupui.edu'
email['caret']= 'caret.cam.ac.uk'
email['gmail']= 'gmail.com'


for hostname in EmaiLista:
    host = str(hostname)
    number = host.find('@')
    HostnameLista.append(hostname[number+1:])

for mail in HostnameLista:
    checker = str(mail)
    if checker.startswith(email['uct']):
        uct_brojac += 1
    elif checker.startswith(email['media']):
        media_brojac += 1
    elif checker.startswith(email['umich']):
        umich_brojac += 1
    elif checker.startswith(email['iupui']):
        iupui_brojac += 1
    elif checker.startswith(email['caret']):
        carnet_brojac += 1
    elif checker.startswith(email['gmail']):
        gmail_brojac += 1
    
print('ucta ima: ', uct_brojac)
print('media ima: ', media_brojac)
print('umich ima: ', umich_brojac)
print('iupui ima: ', iupui_brojac)
print('caret ima: ', carnet_brojac)
print('gmail ima: ', gmail_brojac)