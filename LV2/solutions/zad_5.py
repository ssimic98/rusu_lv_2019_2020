import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


auto = pd.read_csv ('../resources/mtcars.csv')

plt.scatter(auto.mpg, auto.hp,marker='X',c=auto.wt)
plt.colorbar().set_label('Tezina')
plt.xlabel('Potrosnja automobila')
plt.ylabel('Konjska snaga automobila')
plt.show()

print('Maks potrošnja',auto.mpg.max())
print('Min potrošnja',auto.mpg.min())
print('Srednja potrošnja',auto.mpg.mean())
print(auto.hp)