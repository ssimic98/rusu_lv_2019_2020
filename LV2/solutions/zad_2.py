import re
datoteka = '../resources/mbox-short.txt'
emailovi = []

fhand = open(datoteka)
for linija in fhand:
    pomocna = re.findall('[a-zA-Z0-9_.]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+', linija)
    for pomocnik in pomocna:
        emailovi.append(pomocnik.split("@")[0])

prvi = []
for email in emailovi:
    baremjedanA = re.findall('\S*a\S*', email)
    for linija in baremjedanA:
        prvi.append(linija)  
print(prvi)   
  
drugi = []
for email in emailovi:
    SamoJedanA = re.findall('[b-zB-Z0-9]*a[b-zB-Z0-9]+', email)
    for linija in SamoJedanA:
        drugi.append(linija)  
print(drugi)  

treci = []
for email in emailovi:
    NemaA = re.findall('[b-zB-Z0-9]+', email)
    for linija in NemaA:
        treci.append(linija)  
print(treci)  

cetvrti = []
for email in emailovi:
    sadrziZnamenke = re.findall('\S+[0-9]+\S\S+', email)
    for linija in sadrziZnamenke:
        cetvrti.append(linija)  
print(cetvrti)  

peti = []
for email in emailovi:
    malaSlova = re.findall('\S+[^A-Z]+', email)
    for linija in malaSlova:
        peti.append(linija)  
print(peti)  
