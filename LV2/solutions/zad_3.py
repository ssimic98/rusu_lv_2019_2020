import numpy as np
import matplotlib.pyplot as plt

np.random.seed(56) 
randomNumbers=np.random.randint(2,size=1000)

visinaZene=[]
visinaMuski=[]
mu = 3
sigma = 2
v = np.random.normal(mu,sigma,10000)

for i in randomNumbers:
    if i==1:
        visinaMuski.append(np.random.normal(180,7))
    else:
        visinaZene.append(np.random.normal(167,7))

plt.hist([visinaMuski,visinaZene], color=['blue','red'])


avg_muski=np.average(visinaMuski)
avg_zene=np.average(visinaZene)
plt.axvline(avg_muski,color='purple')
plt.axvline(avg_zene,color='black')
