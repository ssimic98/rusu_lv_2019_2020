### Opis dataseta mtcars
Motor Trend Car Road Tests

The data was extracted from the 1974 Motor Trend US magazine, and comprises fuel consumption and 10 aspects of automobile design and performance for 32 automobiles (1973–74 models
* mpg - Miles/(US) gallon
* cyl - Number of cylinders
* disp - Displacement (cu.in.)
* hp - Gross horsepower
* drat - Rear axle ratio
* wt - Weight (lb/1000)
* qsec - 1/4 mile time
* vs - V/S
* am - Transmission (0 = automatic, 1 = manual)
* gear - Number of forward gears
* carb - Number of carburetors

Source: R built in dataset

U ovoj vježbi upoznali smo se sa radom biblioteke re koja sadrži implementaciju regularnih izraza. Unutar te biblioteke koristili smo metodu finadall koja nam omogućuje
izdvajanje dijelova niza koji zadovoljavaju dani regularni izraz. Također smo koristil Numpy biblioteku za stvaranje nasumičnih brojeva kao i korištenje normalne distribucije.
Koristili smo i Matplotlib za grafički prikaz rezultata.
