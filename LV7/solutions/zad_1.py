import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix


# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

# TODO: prikazi nekoliko slika iz train skupa
plt.figure()
plt.imshow(x_train[1,:])
plt.figure()
plt.imshow(x_train[2,:])
plt.figure()
plt.imshow(x_train[3,:])

# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)


# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu
model = keras.Sequential()
model.add(keras.Input(input_shape))
model.add(layers.Flatten())
model.add(layers.Dense(32, activation = 'relu', ))
model.add(layers.Dense(10,activation = 'softmax'))
model.summary()

# TODO: definiraj karakteristike procesa ucenja pomocu .compile()
model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])


# TODO: provedi ucenje mreze
model.fit(x_train_s, y_train_s, batch_size=128, epochs=10,  verbose=1, validation_data=(x_test_s, y_test_s))

# TODO: Prikazi test accuracy i matricu zabune
TestAccuaracy = model.evaluate(x_test_s, y_test_s)
print('Test accuracy:', TestAccuaracy[1])

TrainAccuaracy = model.evaluate(x_train_s, y_train_s)
print('Train accuracy:', TrainAccuaracy[1])



classes = model.predict(x_test_s, batch_size=128)
y_pred = (classes > 0.5)
confusionMatrix = confusion_matrix(y_test_s.argmax(axis=1), y_pred.argmax(axis=1))
print(confusionMatrix)


# TODO: spremi model
model.save("model.h5")

# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu
modelConvolution = keras.Sequential()
modelConvolution.add(layers.Input(shape=(28,28,1), name="ulaz"))
modelConvolution.add(layers.Conv2D(32, kernel_size=(3,3), padding='same', activation="relu"))
modelConvolution.add(layers.MaxPool2D(pool_size=(2,2), strides=(2,2)))
modelConvolution.add(layers.Flatten())
modelConvolution.add(layers.Dense(50, activation="relu"))
modelConvolution.add(layers.Dense(10, activation="softmax", name="izlaz"))
modelConvolution.summary()

# TODO: definiraj karakteristike procesa ucenja pomocu .compile()
modelConvolution.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])


# TODO: provedi ucenje mreze
modelConvolution.fit(x_train_s, y_train_s, batch_size=128, epochs=10,  verbose=1, validation_data=(x_test_s, y_test_s))

# TODO: Prikazi test accuracy i matricu zabune
TestAccuaracyConv = modelConvolution.evaluate(x_test_s, y_test_s)
print('Test accuracy:', TestAccuaracyConv[1])

TrainAccuaracyConv = modelConvolution.evaluate(x_train_s, y_train_s)
print('Train accuracy:', TrainAccuaracyConv[1])



classes = modelConvolution.predict(x_test_s, batch_size=128)
y_pred = (classes > 0.5)
confusionMatrix = confusion_matrix(y_test_s.argmax(axis=1), y_pred.argmax(axis=1))
print(confusionMatrix)


# TODO: spremi model
modelConvolution.save("modelConvolution.h5")
